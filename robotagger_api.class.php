<?php

/**
 * @file
 *
 * Can be used to requests the RoboTagger web service to analyse text or
 * to use some usefully static methods
 *
 * @see robotagger_api_call_webservice().
 * @see http://robotagger.com
 */

class Robotagger {

  /**
   * Contains the response from drupal_http_request() and
   * is set via Robotagger::__analyze().
   *
   * @var array
   */
  private $response = array();

  /**
   * Contains the data which is set via Robotagger::__format() and
   * returned via robotagger_api_call_webservice().
   *
   * @var array
   */
  public $data = array();

  /**
   * Analyzes the text and prepares the data.
   *
   * @param string $content
   *   The content which should be analyzed.
   * @param array $params
   *   The annotation types.
   * @param string $lang
   *   The langcode example de.
   * @param array $topics
   *   An array of topics.
   *
   * @see robotagger_api_call_webservice().
   *
   * @return The Robotagger object.
   */
  public function analyze($content, $params, $lang, $topics = array()) {
    if (empty($content)) {
      return;
    }
    $this->__analyze($content, $params, $lang, $topics);
    $this->__format();
    return $this;
  }

  /**
   * The private methode which do the magic to request webservice.
   *
   * @param string $content
   * @param array $params
   * @param string $lang
   * @param array $topics
   *
   * @see Robotagger::analyze().
   *
   * @return The Robotagger object.
   */
  private function __analyze($content, $params, $lang, $topics) {
    if (!empty($topics)) {
      $topics = array_map('trim', $topics);
      $topics = implode('/', $topics);
    }
    else{
      $topics = '';
    }
    // prepare annotypes
    ob_start();
    foreach ($params as $annotype) include 'robotagger_api.annotype.xml.php';
    $annotypes = ob_get_contents();
    ob_end_clean();

    // prepare xml data
    $data = array(
      'api_key' => variable_get('robotaggerapi_server_apikey', ''),
      'lang' => $lang,
      'annotypes' => $annotypes,
      'content' => check_plain($content),
      'topic' => $topics
    );
    // generate xml
    ob_start();
    include 'robotagger_api.xml.php';
    $xml = ob_get_contents();
    ob_end_clean();
    try {
      // query
      $headers  = array('Content-Type' => 'application/x-www-form-urlencoded');
      $data_enc = http_build_query(array('rtXMLRequest' => $xml), '', '&');
      $this->response = drupal_http_request(self::gethost(), array('headers' => $headers, 'method' => 'POST', 'data' => $data_enc, 'max_redirects' => 3));
      // check for errors
      if (isset($this->response->error)) {
        self::log_error($this->response->code, $this->response->error);
        return;
      }
    } catch (Exception $err) {
      return;
    }
    return $this;
  }

  /**
   * Prepares the response data which is stored in Robotagger::response.
   *
   * @return The Robotagger object.
   */
  private function __format(){
    try {
      if (empty($this->response->data) || !empty($this->response->error)) {
        return;
      }
      // See http://drupal.org/node/1781878.
      if (function_exists('querypath_include_code')) {
        querypath_include_code();
      }
      $result = array();
      $error = qp($this->response->data)->find('error')->text();
      if (!empty($error)) {
        $this->response->error = $error;
        self::log_error($this->response->code, $error);
        return;
      }
      $xml = qp($this->response->data);
      $result['topic'] = qp($this->response->data)->find('topic')->text();
      $children = $xml->children();
      $DOMElements = $children->get();
      foreach ($DOMElements as $DOMElement) {
        $DOMNodeLists = $DOMElement->getElementsByTagName('annotation');
        foreach ($DOMNodeLists as $DOMNodeList) {
          $anno_type = $DOMNodeList->getAttribute('annoType');
          $result[$anno_type][] = array(
            'value' => $DOMNodeList->getAttribute('stringVal'),
            'occurences' => $DOMNodeList->getAttribute('occurrences'),
            'threshold' => $DOMNodeList->getAttribute('threshold'),
            'subtype' => $DOMNodeList->getAttribute('subtype'),
          );
        }
      }
      $this->data = $result;
      return $this;
    }
    catch (Exception $e) {
      return $this;
    }
  }

  /**
   * Returns the stored uri from RoboTagger-Webservice.
   *
   * @see robotagger_api_settings_form
   *
   * @return string
   *   The stored uri from RoboTagger-Webservice.
   */
  public static function gethost() {
    return variable_get('robotaggerapi_server', 'http://ws.robotagger.com');
  }

  /**
   * Logs an error message via watchdog() and print it via drupal_set_message().
   *
   * @param int $code
   *   A integer code which is set by drupal_http_request().
   * @param string $error
   *   A error message which is set by drupal_http_request().
   */
  private static function log_error($code, $error) {
    drupal_set_message(t('RoboTagger-API processing error: (@code - @error)', array('@code' => $code, '@error' => $error)), 'error', FALSE);
    watchdog('robotagger_api', 'RoboTagger-API processing error: (@code - @error)', array('@code' => $code, '@error' => $error), WATCHDOG_ERROR);
  }

  /**
   * Returns the error code and error message.
   *
   * @see robotagger_api_call_webservice().
   */
  public function getError() {
    if (!empty($this->response->error)) {
      return array('errorcode' => $this->response->code, 'errormessage' => $this->response->error);
    }
    return;
  }

  /**
   * Checks that the api-key is valid or not.
   *
   * @param string $key
   *   The api-key for the webservice.
   * @param string $host
   *   The host uri for the webservice.
   *
   * @see robotagger_api_validate_api_key(). Please use this function.
   * @see http://robotagger.com
   *
   * @return mixed
   *   - returns -1 when the response body is not the expected response
   *   - returns -2 at any error in the response
   *   - returns 0 if the key is not valid
   *   - returns 1 if the key is valid
   */
  public static function validate_api_key($key, $host) {
    if (empty($host)) {
      $host = self::gethost();
    }
    $url = url($host, array('query' => array('check_apikey' => $key)));
    $response = drupal_http_request($url);
    $expected_response = array('1', '0');
    $data = isset($response->data) ? trim($response->data, "\n") : '';
    if ($data === '' || !in_array($data, $expected_response, TRUE)) {
      self::log_error(t("Webservice-URL isn't correct or you may have problems with your internet connection"), $response->code);
      return -1;
    }
    if (!empty($response->error)) {
      self::log_error($response->error, $response->code);
      return -2;
    }
    return $data;
  }

  /**
   * Returns an numeric array annotypes and subtypes of annotype.
   *
   * @param string $langcode
   *   One of the supported language. Ex. de.
   *
   * @return array
   *   An numeric array with annotypes and subtypes.
   */
  public static function get_annotypes_and_subtypes($langcode) {
    $return = array();
    $annotypes = self::get_annotypes($langcode);
    $subtypes = array();
    foreach ($annotypes as $annotype) {
      $key = trim(drupal_strtolower($annotype['name']));
      $annotypes_new[$key] = $annotype['name'];
      if (!empty($annotype['subtypes'])) {
        foreach ($annotype['subtypes'] as $subtype) {
          $subtypes[$key] = $subtype['values'];
        }
      }
    }
    return array($annotypes_new, $subtypes);
  }

  /**
   * Returns an array of annotations which than is used as toxonomy terms.
   *
   * @param string $langcode
   *   One of the supported languages. Ex. de.
   *
   * @see robotagger_api_get_annotype_names(). Please use this function.
   *
   * @return array $annotypes
   *   An numeric array which contains an array with the keys name and
   *   description of a annotation.
   */
  public static function get_annotype_names($langcode) {
    $annotypes = array();
    $robotagger_annotypes = self::get_annotypes($langcode);
    foreach ($robotagger_annotypes as $robotagger_annotype) {
      $annotypes[] = array('name' => $robotagger_annotype['name'], 'description' => $robotagger_annotype['description']);
    }
    return $annotypes;
  }

  /**
   * Requests the RoboTagger web service to get a list of supported annotypes and
   * his subtypes.
   *
   * @param string $langcode
   *   One of the supported language. Ex. de.
   *
   * @see robotagger_api_get_annotypes() Please use this funtion.
   *
   * @return array $vocs
   *   An numeric array of annotationtypes with the keys name, description
   *   and subtypes. Which contains an array of subtypes which also has a name,
   *   description and an array of strings.
   */
  public static function get_annotypes($langcode) {
    if (($cache = cache_get('robotagger_api_vocs')) && !empty($cache->data)) {
      return $cache->data;
    }
    $url = url(self::gethost() , array('query' => array('get_annotypes' => 1)));
    $response = drupal_http_request($url);
    if (!empty($response->error)) {
      self::log_error($response->code, $response->error);
      return array();
    }
    $vocs = array();
    // See http://drupal.org/node/1781878.
    if (function_exists('querypath_include_code')) {
      querypath_include_code();
    }
    $qp = new QueryPath($response->data, NULL, array('replace_entities' => TRUE));
    $children = $qp->children();
    $annotation_types = $children->toArray();
    foreach ($annotation_types as $i => $annotation_type) {
      $name = $annotation_type->getAttribute('name');
      $voc = array();
      $voc['name'] = trim($name);
      $descriptions = $annotation_type->getElementsByTagName('description');
      $voc['description'] = '';
      if (!empty($descriptions)) {
        $voc['description'] = $descriptions->item(0)->nodeValue;
      }
      $voc['subtypes'] = array();
      $annotation_subtypes = $annotation_type->getElementsByTagName('annotation_subtype');
      foreach ($annotation_subtypes as $annotation_subtype) {
        $subtype = array();
        if (is_object($annotation_subtype)) {
          $subtypename = $annotation_subtype->getAttribute('name');
          $subtype['name'] = trim($subtypename);
          $subtype_values = $annotation_subtype->getElementsByTagName('subtype_value');
          $descriptions = $annotation_subtype->getElementsByTagName('description');
          $subtype['description'] = '';
          if (!empty($descriptions)) {
            $subtype['description'] = $descriptions->item(0)->nodeValue;
          }
          $subtype['values'] = array();
          if (is_object($subtype_values)) {
            foreach ($subtype_values as $subtype_value) {
              $value = trim($subtype_value->nodeValue);
              $subtype['values'][drupal_strtolower($value)] = $value;
            }
          }
        }
        $voc['subtypes'][] = $subtype;
      }//foreach ($annotation_subtypes as $annotation_subtype) {
      $vocs[$voc['name']] = $voc;
    }//foreach ($annotation_types as $i => $annotation_type) {
    if (!empty($vocs)) {
      $expire = time() + 60*60*24*7;
      cache_set('robotagger_api_vocs', $vocs, 'cache', $expire);
    }
    return $vocs;
  }

  /**
   * Requests the RoboTagger web service and returns an numeric array of topics.
   *
   * @see http://robotagger.com
   *
   * @return array $topics
   *   An numeric array of topics.
   */
  public static function getTopics() {
    $response = drupal_http_request(self::gethost() . '?get_topics');
    if (!empty($response->error)) {
      self::log_error($response->code, $response->error);
      return array();
    }
    $topics = array();
    // See http://drupal.org/node/1781878.
    if (function_exists('querypath_include_code')) {
      querypath_include_code();
    }
    foreach ($xml = qp($response->data)->find('topic') as $topic) {
      $topics[] = $topic->text();
    }
    return $topics;
  }
}
