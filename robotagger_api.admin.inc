<?php

/**
 * @file Contains functions to build the settings page.
 */

/**
 * Renders the settings page.
 */
function robotagger_api_settings_page() {
  $output = '';
  $form = drupal_get_form('robotagger_api_settings_form');
  $output .= drupal_render($form);
  return $output;
}

/**
 * Builds the settings form.
 *
 * @see system_settings_form().
 */
function robotagger_api_settings_form($form, $form_state) {
  $form['robotaggerapi_server'] = array(
    '#type' => 'textfield',
    '#title' => t('RoboTagger server host'),
    '#default_value' => Robotagger::gethost(),
    '#required' => TRUE,
  );
  $form['robotaggerapi_server_apikey'] = array(
    '#type' => 'textfield',
    '#title' => t('RoboTagger server api-key'),
    '#default_value' => variable_get('robotaggerapi_server_apikey', ''),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}

/**
 * Validation handler for robotagger_api_settings_form().
 */
function robotagger_api_settings_form_validate($form, $form_state) {
  if (empty($form_state['values']['robotaggerapi_server_apikey'])) {
    return;
  }
  $response = robotagger_api_validate_api_key($form_state['values']['robotaggerapi_server_apikey'], $form_state['values']['robotaggerapi_server']);
  if ($response === '1') {
    return TRUE;
  }
  if ($response === '0') {
    form_set_error('robotaggerapi_server_apikey', t('Your RoboTagger api-key is not valid.'));
    return FALSE;
  }
  return form_set_error('', '');
}
